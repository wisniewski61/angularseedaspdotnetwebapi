﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AngularSeedWebApi.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            string url = HttpContext.Request.Url.AbsoluteUri;
            string path = HttpContext.Request.Url.AbsolutePath;

            // if path is eg. /home/index redirect to /
            if (!path.Equals("/"))
            {
                string redirect = url.Substring(0, url.IndexOf(path) + 1);
                Response.Redirect(redirect);
            }//if

            return View();
        }

        public ActionResult view1()
        {
            return View();
        }

        public ActionResult view2()
        {
            return View();
        }
    }
}
