# README #

This is simply ASP.NET WebAPI project (standard WebAPI template with authentication - Individial User Accounts) that includes AngularJS and some other Javascript libraries.
This project is based on original [Angular Seed](https://github.com/angular/angular-seed), but it doesn't support unit tests and loading all of the dependencies asynchronously.
This project uses Bower to manage front-end libraries and NuGet to manage back-end libraries.

# Getting started #

## Requirements: ##

* Visual Studio 2015 (it might work in older versions too, but there is no warranty)
* .NET Fremerwork 4.5.2 or newer (again you can try older versions)
* NodeJS 4.6
* Bower

## Run the Application: ##

1.   Open solution in Visual Studio.
2.   Right-click on Solution and restore NuGet Packages.
3.   Right-click on package.json and restore packages.
4.   Right-click on bower.json and restore packages.
5.   Run project.

# License #

See LICENSE file.